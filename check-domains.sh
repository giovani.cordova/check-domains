#!/bin/bash

dt=`date +"%Y/%m/%d"`

while read line
do
        domain=$line
        expires=`whois $line | grep expires | awk '{print $2}'`
        dtExpires=`date -d "$expires" +'%Y/%m/%d'`
        
        d1=$(date -d "$dt" +%s)
        d2=$(date -d "$dtExpires" +%s)

        diff_secs=$((d2 - d1))
        diff_days=$((diff_secs / (24 * 60 * 60)))

        echo -e "Verificando dominio \033[0;32m$line\033[0m..."
        echo -e "Faltam \033[0;32m$diff_days\033[0m para o dominio \033[0;32m$line\033[0m expirar."

        if [ "$diff_days" -lt '30' ]; then
                echo "O Dominio $line esta proximo de expirar."
                curl -s 'https://api.callmebot.com/whatsapp.php?phone=55XXXXXXXXX6&text=Verificar+o+dominio+'$line'+esta+a+'$days'+dias+para+expirar&apikey=XXXXX' > /dev/null
        fi
        
        echo
        echo
done <<< "`cat list`"
